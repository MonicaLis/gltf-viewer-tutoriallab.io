---
id: gltf-viewer-01-intro-02-sdk
title: Software Development Kit
---

This tutorial is based on the companion repository [https://gitlab.com/gltf-viewer-tutorial/gltf-viewer](https://gitlab.com/gltf-viewer-tutorial/gltf-viewer). It contains a basic skeleton for the application, third party libraries and some utilities to build, run and debug the code.

## Quick introduction to Git

Git is a software to handle versioning and collaboration on a repository. Most often it is used for source code, but it can be used to keep an history of modifications on any kind of text files.

Using such tool is almost mandatory for software development: it gives us security, history of changes and easy collaboration.

For this tutorial we will use Gitlab, but other providers exist such as [https://bitbucket.org/](https://bitbucket.org/) and [https://github.com/](https://github.com/).

If you are confident with Git and if you prefer to use another platform, feel free to do as you like.

## Creating a Gitlab account

If you don't have one already, create a Gitlab account on [https://gitlab.com/](https://gitlab.com/) and log in to your account.

## Forking the repository

Fork the repository: [gltf-viewer](https://gitlab.com/gltf-viewer-tutorial/gltf-viewer). It can be done with the "Fork" button, top-right on the page.

Forking means that you create a copy of the repository on your own account. The platform keeps track of fork relationships between repository.

With this copy, you will have the opportunity to do whatever modifications you want and push them online for backup or collaboration with other people.

## Installing Git

If Git is not already installed on your system, install it.

On Linux, use your system package manager (apt for Ubuntu, pacman for Archlinux, etc).

On Windows, you can install Git for Windows at this URL: https://git-scm.com/download/win. It will install Git on your system but also a bash terminal emulator that will allow you to use the same commands as for Linux.

## Clone your fork

Use the following command to clone your fork in a local folder named `gltf-viewer`:

```bash
git clone URL_TO_YOUR_FORK
cd gltf-viewer
```

The base code template is at the tip of git branch `tutorial-v1`, so ensure you are on this branch using the command:

```bash
git checkout tutorial-v1 # Ensure we start from branch tutorial-v1
```

## How to commit and push your code

In the tutorial I will ask you to commit and push your code to Github. The commands to use for that are:

```bash
git pull # Optional, to be sure you are up to date if you have other contributors
git add . # Add all modification
git commit -m "A message describing your changes" # Commit locally your modifications
git push # Push to the remote repository
```

## Building the project

At this point we should be able to build the project with CMake. CMake is a tool that is used to generate build files. It is useful for multi-platform projects to avoid dealing with different build system (Makefile, Visual Studio solutions, etc.).

A CMake project can easily be spotted if you see a CMakeLists.txt file at the root of the project. This is the file that should be written to tell CMake how to build the project (what files to compile, what executables/libraries to build, etc).

Most of C/C++ projects are now written with a CMakeLists.txt file. It has became the main build solution of the C++ ecosystem, so it is important to know how to use it.

### Install CMake

If you don't have CMake on your system, you need to install it.

On Linux, use your system package manager (apt for Ubuntu, pacman for Archlinux, etc).

On Windows, you can download the installer CMake [at this URL](https://cmake.org/download/). During the installation procedure, check the box that asks if you want to put CMake in your PATH variable. You will need to restart your terminal for this change to take effect.

### Use CMake

When using CMake, we have 2 steps:
1. Configure the project with cmake - It generates the build solution (Makefiles for gcc, Visual Studio project for VS, etc) in a build folder
2. Compile the project with the native tool

The file `.vscode/bash-init.sh` contains a few bash functions to run the most common cmake commands we will use. To use them:

```bash
source .vscode/bash-init.sh # 'Import' all functions in current bash session
cmake_prepare # Configure the project in `build` subfolder
cmake_build # Build the project
cmake_install # Extract portable installation in `dist` subfolder
```

By default the build should be done in Debug mode, so you can use GDB or Visual Studio debugger to debug it. If you want to build in release for better performance:

On Linux:
```bash
cmake_prepare -DCMAKE_BUILD_TYPE=Release
cmake_build
```

On Windows:
```bash
cmake_prepare
cmake_build --config release
```

You might have issues if your CMake version is too old. I'm using 3.19.2, try to use at least the same. If you can't, you can use standard `make` command to build your project on Linux:

```bash
cmake_prepare
cd build
make -j
```

## Run the executable

Now that it is compiled, you should be able to run:

```bash
./build/bin/gltf-viewer info
```

which should display some information about your OpenGL version.

On windows you might need to add Debug folder in the path (or Release if you build in Release config):

```bash
./build/bin/Debug/gltf-viewer info
```

## Base code

On order for you to quickly start, I have made a code template that must be completed during this tutorial.

The `main.cpp` file is already complete and just parse the arguments on the command line to forward them to the ViewerApplication class.

The ViewerApplication class has a constructor and a run() method. Most of the code should be implemented in the run() method, where I put comments of the form `// TODO ...`.

Just follow the tutorial and you will know when to replace what.

## Test scenes and commands

The `.vscode/bash-init.sh` also contains commands to download test files. If you have enough disk space, you can get all glTF sample models with:

```bash
clone_gltf_samples # Warning: more than 1GB
```

Otherwise, you can download these two models:

```bash
get_damaged_helmet_sample # Quite small
get_sponza_sample # A bit larger
```

The following utility commands allows to run the viewer of these two files:

```bash
view_helmet
view_sponza
```

But don't expect to see anything but black before the end of the tutorial part "Loading and drawing".

## Going further

### Git

You can find more information about Git [here](https://git-scm.com/), and thousands of tutorials online. I strongly advise you to learn how to do basic tasks on git from the command line such as:

- create a new repository
- cloning a repository
- attach a local repository to an online repository
- create branches, switch branches
- commit changes on a branch
- pushing and pulling changes
- merge branches
- use pull requests to send modifications from one repository to another

It is also possible to use graphical editors for Git, such as VS Code (with good extensions such as GitLens and Git Graph), Fork or Sublime Merge, but knowing how to do them on the command line is always good.

### CMake

CMake can be a bit hard to learn "correctly" because it has evolved a lot in a few years. There is the old way of doing things, and the new way. When you search for tutorial, try to search for "modern cmake" instead of just "cmake". In the meantime, you can use the following resources that are recent:

- [Oh No! More Modern CMake - Deniz Bahadir - Meeting C++ 2019](https://www.youtube.com/watch?v=y9kSr5enrSk)
- [Modern CMake Book](https://cliutils.gitlab.io/modern-cmake/)

Note that the Meeting C++ conference seems to feature a new talk each year about CMake, so you have no excuse to be up to date (ok I admit I didn't saw the last one, so my CMake code may be crap, I'll fix it later ^^').

Also keep an eye on the CMake version you are using. If it is your system you should have installed the last one. Otherwise you need to check the version with `cmake --version` and only use CMake commands that are allowed for your version (or ask the system administrator to update the version).
