---
id: gltf-viewer-01-intro-01-intro
title: Introduction
---

Welcome to this tutorial ! It's goal is to teach you OpenGL by creating a complete project: a glTF 3D Viewer.

## Why a viewer ?

Coding a 3D viewer to learn OpenGL has several advantages:

- Adaptive level of difficulty for learning
- Focus on 3D rendering
- Knowledge acquired is likely to be useful if you work in 3D programming
- Easy to extend with new rendering techniques
- Can become your 3D hub to try new things, build prototypes, etc.
- More fun than drawing cubes and spheres

## What is glTF ?

glTF is a file format specified by the Khronos Group, which is also responsible for the specification of OpenGL.

Here is a direct citation from the [official glTF repository](https://github.com/KhronosGroup/glTF) where the specification is hosted:

> glTF (GL Transmission Format) is a royalty-free specification for the efficient transmission and loading of 3D scenes and models by applications. glTF minimizes both the size of 3D assets, and the runtime processing needed to unpack and use those assets. glTF defines an extensible, common publishing format for 3D content tools and services that streamlines authoring workflows and enables interoperable use of content across the industry.

Our goal is to code a viewer for these type of files, using OpenGL and C++.

## Why glTF ?

There is many 3D file format out there and most often people starts with the OBJ file format. But nowadays I think glTF is a better starting point for loading 3D model. Indeed:

- It has a good specification
- It can be extended in a way that is formalized by the specification
- It has by default a modern material model that is often used in real-time rendering softwares (games or applications)
- It has been built with OpenGL in mind: the data layout closely follows how the data is stored with OpenGL and how rendering is done
- Khronos provides a repository with many sample models to test our application: [glTF-Sample-Models](https://github.com/KhronosGroup/glTF-Sample-Models)
- There is also a web sample viewer to view in 3D the sample models: [glTF-Sample-Viewer](https://github.khronos.org/glTF-Sample-Viewer/). Its source code is available so we can compare our code to its if we don't have expected results.
- [Sketchfab](https://sketchfab.com/), a platform to share 3D models, propose auto-conversion to glTF for all its models. Many models are free so it gives us a large library of 3D models to play with our viewer.
- It has an [official Blender importer and exporter](https://github.com/KhronosGroup/glTF-Blender-IO) which can be great to create your own assets for free.

## Prerequisites

For now this tutorial assume you already have the following basic knowledge:

- **OpenGL 3+** You should know what are buffer objects, vertex array objects, textures, shaders. The tutorial will explain how to create and manipulate this objects but will not dive into details about them. However I'll link wiki or documentation pages related to these concepts.
- **C++** You should be familiar with basic C++ syntax and semantics, know how to write a function or a class, etc. I'll introduce more advanced concepts as needed. If you think something is not clear don't hesite to contact me and I'll update the tutorial to provide more details.

## What are we going to learn ?

This tutorial covers many aspects of OpenGL and C++ development, but also code versionning and collaboration with Git, CMake and test driven development. Here is a coarse list of concepts we will cover:

- Forking, cloning and collaborating on git repositories
- Compiling a C++ project using CMake
- Create a graphical application
- Understand and manipulate glTF format
- Use OpenGL to render a 3D scene described by a glTF file
- Camera control
- Simple directional lighting
- Physically based materials rendering

## Operating System

This tutorial has been written for Linux and Windows. I mainly code on Windows, so there may be issues for Linux.

On Linux I use gcc, and on Windows I use Visual Studio Community 2019 (more specifically, I use Visual Studio Code for code edition and only the compiler of Visual Studio Community 2019).

## Asking questions, contributing

You can ask questions by [opening an issue on the tutorial repository](https://gitlab.com/gltf-viewer-tutorial/gltf-viewer-tutorial.gitlab.io/-/issues).

If you spot a mistake, you can fix it and [send me a merge request](https://gitlab.com/gltf-viewer-tutorial/gltf-viewer-tutorial.gitlab.io/-/merge_requests).
