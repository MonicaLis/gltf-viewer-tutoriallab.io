---
id: gltf-viewer-01-intro-03-devenv
title: Developer Environment
---

This part is optional and only gives some advice for your developer environment, especially if you want to be multi-platform. I present here my own developer environment.

## Compiler

On Linux I use gcc, the most recent version if possible.

On Windows I use Visual Studio Community 2019, which can be downloaded from the microsoft website. However I don't use the IDE, only the compiler, and I build from command line with `cmake --build`.

Another good choice can be clang I guess, which is multi-platform, but I never tried it.

## Editor

I use VS Code because:

- Open source, but maintained by a very active team from Microsoft
- Multiplatform
- Built with web technologies, but quite fast
- Extensibility with extensions
- A lot of extensions already available
- Integration with debuggers
- Multi languages
- Native markdown support

The C/C++ extension of VS Code is now mature enough, in my opinion, to stop using heavy IDEs like Visual Studio.

## VS Code Extensions

The code template provides recommanded extensions in `.vscode/extensions.json`

## Windows Dev Env, what other tools ?

- MSYS2 is my terminal
- Cmder is my terminal launcher (quake style dropdown ftw)
- Scoop is my installation manager (if a software is available in Scoop, I install it with Scoop)
- Everything is my file searcher
